import json
import requests

# Record your Account UUID here:
# (See http://mathz-blastererz.org/docs for details)
account_uuid = ""

mathz_api = 'http://mathz-blastererz.org/api/mathz'


def play_mathz_blastererz():
    json_data = None
    game_state = ""

    while 'incorrect' not in game_state.lower():
        json_data = mathz_post()
        game_state = json_data['state']

        answer = process_question(json_data)

        json_data = mathz_post(answer)
        game_state = json_data['state']
        print("   Outcome:", game_state)

    print()
    print("Final State:", json.dumps(json_data, indent=3))


def process_question(json_data):
    level = json_data['current_level']
    question = json_data['question']
    notation = json_data['notation']
    print(f"Level {level} - {notation} notation:")
    print("   Question: ", question)

    # TODO: Implement better question answering
    answer = 2

    print("   Answer: ", answer)
    return answer


def mathz_post(answer=None):
    data = {'account_uuid': account_uuid}

    if answer is not None:
        data['answer'] = answer

    response = requests.post(mathz_api, json=data)

    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError:
        print("Mathz Blastererz post failed!")
        print("Error:", response.json().get("detail") or response.text)
        raise

    return response.json()


HELP_TEXT = """
Register an account for your Team, and record the resulting
account_uuid in this file, on line 9.
​
(See http://mathz-blastererz.org/docs for details)
"""


def main():
    if not account_uuid:
        print(HELP_TEXT)
        return

    play_mathz_blastererz()


if __name__ == '__main__':
    main()
