# Mathz Blastererz Client

A minimal starter client for the [Mathz Blastererz](http://mathz-blastererz.org/) challenge.

## Requirements

It currently requires Python 3, and the `requests` library. You can install it using pip (e.g. `pip install requirements`), ideally in a virtual environment.

## Setup

To get started, register an account with the Mathz Blastererz API to obtain an Acount UID. You could use a command line utility like `curl`, or the `requests` library in Python, but the quickest way is probably to use the tools in the [OpenAPI docs](http://mathz-blastererz.org/) themselves:

* Click the `POST` /api/account" endpoint to expand it

* Click the `Try It Out` button

* In the sample JSON submission, enter a name for your Team's account (replacing the word "string")

* Click the `Execute` button

* Scroll down to the resulting Server Response, and copy the "uuid" value: This is your new Account ID. (If it failed, you probably need to choose a different name.)

* Insert your new Account ID into the script on line 9, as the value of `account_uuid`

You should now be able to run the script:

```
python play-math-blastererz.py
```

And see it attempt to answer the first question, and probably get it wrong.

The initial implementation (always answering "2") is... not good. How could you improve it?
